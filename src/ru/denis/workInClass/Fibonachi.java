package ru.denis.workInClass;

import java.util.Scanner;

/**
 * программа для поиска числа фибоначчи
 *
 * @author - Denis Leontev
 */
public class Fibonachi {
    public static void main(String[] args) {
        long before = System.currentTimeMillis();
        long f1 = 1; // первое число
        long f2 = 1; // второе число
        long temp;     // временная переменная куда будем сохранять результат
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число итераций для поиска числа фабиначи - ");
        long iterations = scan.nextLong();
        System.out.print(f1 + " " + f2 + " ");
        for (long i = 3; i <= iterations; i++) {
            temp = f1 + f2; // формула поиска числа
            System.out.print(temp + " ");
            f1 = f2;
            f2 = temp;
        }
        System.out.println();
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд");
    }
}