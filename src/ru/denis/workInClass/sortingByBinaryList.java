package src.ru.denis.workInClass;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Программа поиска индекса отсортированного массива методом бинарного поиска используя ArrayList.
 * @author Denis Leontev.
 */
public class sortingByBinaryList {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20));
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите  число для поиска его индекса в массиве - ");
        int number_inArray = scan.nextInt();
        if(Binary(arrayList,number_inArray) == -1) {
            System.out.println("Нужного числа нет в массиве :( ");
        } else {
            System.out.println("Нужный индекс массива найден - " + Binary(arrayList,number_inArray));
        }
    }

    /**
     * Реализация метода поиска индекса нужного элемената методом бинарного поиска.
     * @param arrayList - массив чисел нужный для обработки , типа ArrayList.
     * @param number_inArray - число , у которого нужно найти индекс.
     * @return - возвращает -1 для сравнения в цикле , который реализован в main.
     */
    private static int Binary(ArrayList<Integer> arrayList, int number_inArray) {
        int number = -1;                                                       // переменная для найденного индекса
        int first_index = 0;                                                   // минимальный элемент массива
        int last_index = arrayList.size() - 1;                                 // последний элемент массива
        while (first_index <= last_index) {
            int mid_array = (first_index + last_index) / 2;
            if (arrayList.get(mid_array) < number_inArray) {
                first_index = mid_array + 1;
            } else if (arrayList.get(mid_array) > number_inArray) {
                last_index = mid_array - 1;
            } else if (arrayList.get(mid_array) == number_inArray) {
                number = mid_array ;
                break;
            }
        }
        return number;
    }
}
