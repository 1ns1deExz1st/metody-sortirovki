package ru.denis.workInClass;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * реализация программы поиска индекса заданного числа бинарным поиском
 * массив чисел {@code array}
 * @author Denis Leontev and Kozhenenko Daniil
 */
public class sortingByBinary {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
       // ArrayList<Integer> arrayList = new ArrayList<>();

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите  число для поиска его индекса в массиве - ");
        int number_inArray = scan.nextInt();
        if(Binary(array,number_inArray) == -1) {
            System.out.println("Нужного числа нет в массиве :( ");
        } else {
            System.out.println("Нужный индекс массива найден - " + Binary(array,number_inArray));
        }
    }

//    private static int BinaryArray(ArrayList<Integer> arrayList, int number_inArray) {
//        int number = -1; // переменная для найденного индекса
//        int first_index = 0; // минимальный элемент массива
//        int last_index = arrayList.size() - 1; // последний элемент массива
//        while (first_index <= last_index) {
//            int mid_array = (first_index + last_index) / 2;
//            if (arrayList.get(mid_array)< number_inArray) {
//                first_index = mid_array + 1;
//            } else if (arrayList.get(mid_array) > number_inArray) {
//                last_index = mid_array - 1;
//            } else if (arrayList.get(mid_array) == number_inArray) {
//                number = mid_array ;
//                break;
//            }
//        }
//        return number;
//    }

    /**
     *
     * @param array - массив для обработки числа.
     * @param number_inArray - передаём число , для поиска его индекса.
     * @return - возвращает -1 если нет нужного индекса в искомом массиве.
     */
    private static int Binary(int[] array, int number_inArray) {
        int number = -1; // переменная для найденного индекса
        int first_index = 0; // минимальный элемент массива
        int last_index = array.length - 1; // последний элемент массива
        while (first_index <= last_index) {
            int mid_array = (first_index + last_index) / 2;
            if (array[mid_array] < number_inArray) {
              first_index = mid_array + 1;
            } else if (array[mid_array] > number_inArray) {
                last_index = mid_array - 1;
            } else if (array[mid_array] == number_inArray) {
                number = mid_array ;
                break;
            }
        }
        return number;
    }
}
