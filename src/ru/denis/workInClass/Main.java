package src.ru.denis.workInClass;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> numbersInArray = new ArrayList<>(Arrays.asList(1,32,4,5,6,7,8));
        System.out.println(numbersInArray);
        insertionSort(numbersInArray);
        System.out.println(numbersInArray);
    }
    private static void insertionSort (ArrayList<Integer> numbersInArray) {
        for ( int out = 1; out < numbersInArray.size(); out++) {
            int temp = numbersInArray.get(out);
            int in = out;
            while(in > 0 && numbersInArray.get(in - 1) >= temp) {
                numbersInArray.set(in, numbersInArray.get((in - 1)));
                in--;
            }
            numbersInArray.set(in, temp);
        }
    }
}
