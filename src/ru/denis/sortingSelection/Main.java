package src.ru.denis.sortingSelection;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] numbers = {7, 3, 2, 8, 6, 10, 133, 25, 876, 123};
        System.out.println(Arrays.toString(numbers));
        System.out.println("Обработанный массив - ");
        selectionSort(numbers);
    }
    private static void selectionSort(int[] Array) {
        for (int i = 0; i < Array.length; i++) {
            int min = Array[i];
            int imin = i;
            for (int j = i + 1; j < Array.length; j++) {
                if (Array[j] < min) {
                    min = Array[j];
                    imin = j;
                }
            }
            System.out.print(Array[i] + " ");
        }
    }
}
