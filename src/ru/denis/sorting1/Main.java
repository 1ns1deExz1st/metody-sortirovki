package src.ru.denis.sorting1;

import java.util.Scanner;

import java.util.Arrays;

/** в классе Main реализован поиск элемента вводимого пользвателем.
 *
 * инициализировали массив и спросили у пользователя какой элемент нужно найти.
 *
 * вывели элементы массива на экран.
 *
 * вызвали метот enterNumber в if и проверели условие.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(Arrays.toString(numbers));
        System.out.println("Введите число для поиска в массиве - ");
        int number = scan.nextInt();
        int indexOfArray = enterNumber(numbers,number);
        if (indexOfArray == -1) {
            System.out.println("Нет данного элемента !");
        } else {
            System.out.println("Найден нужный элемент - " + indexOfArray);
        }
    }

    /** enterNumber - метод поиска вводимого пользователем числа.
     *
     * @param array - передаём в метод массив чисел целочисленного типа.
     * @param number - число вводимое пользователем.
     * @return - возвращает -1 , которое мы в Main проверяем с помощью if.
     */
    private static int enterNumber(int[] array, int number) {
        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                return i;
            }
        }
        return -1;
    }
}
