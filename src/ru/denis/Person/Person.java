package ru.denis.Person;

public class Person {
    private String surname;
    private int birthday;

    Person(String surname, int birthday) {
        this.surname = surname;
        this.birthday = birthday;
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthday() {
        return birthday;
    }

    @Override
    public String toString() {
        return "Персона ( " +
                "Фамилия - '" + surname + '\'' +
                ", День рождения - " + birthday +
                ')';
    }
}
